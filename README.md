**Battleship**

Welcome to my mini implementation of the popular game, battleship. To start playing, just run the following command :

Development mode:` au run`
Or, if you want to make some code updates, maybe you want the live reload browser feature. Just type:` au run --watch`

**Tech stack**

* Webpack
* AureliaJS
* Materialize CSS
* ESLint
* Gulp
* SASS

If you have some doubt about the tools used, please, go to the `doc/arch/***` files to see the Architecture design records of this application.


Enjoy it!!
