export class App {

  configureRouter(config, router) {
    this.router = router;
    config.title = 'Battleship';
    config.map([
      {route: '',      redirect: 'home'},
      {route: ['home'], name: 'home', moduleId: PLATFORM.moduleName('presentation/home/app-home'), nav: true},
      {route: ['game'], name: 'game', moduleId: PLATFORM.moduleName('presentation/game-view/game-view'), title: 'Game', nav: true}
    ]);
  }

}
