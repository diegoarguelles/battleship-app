import {inject} from 'aurelia-framework';
import {MdToastService} from 'aurelia-materialize-bridge';

@inject(MdToastService)
export class GameView {

  player = null;

  rows = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J']
  columns = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
  ships = [4, 3, 3, 2, 2, 2, 1, 1, 1, 1];
  shipsPositions = []
  orientation = ['vertical', 'horizontal']
  battleground = []
  battlegroundSolution = []
  showSolution = false;

  successMessages = ['Nice shoot!', 'Good!', 'The devil is released!', 'Awesome!'];
  failMessages = ['Oops!', 'Try to make it better', 'Bad shoot', 'ZzzZzzz!'];

  constructor(toast) {
    this.toast = toast;
  }

  attached() {
    //retrieve current player info from local storage
    this.player = JSON.parse(window.localStorage.getItem('gameInfo'));
    this.createBattleground();
  }

  defineShipLocations() {
    let context = this;
    context.ships.map(ship => {
      context.putShipOnBattleground(context, ship);
    });
  }

  putShipOnBattleground(context, ship) {
    let orientation = context.orientation[context.getRandomInt(0, 2)];
    let shipRow;
    let shipColumn;
    let positions = []

    if (orientation == 'vertical') {
      shipRow = context.getRandomInt(0, (9 - ship));
      shipColumn = context.getRandomInt(1, (context.columns.length - 1));
      for (let i = 0; i < ship; i++) {
        let position = {'row': context.rows[shipRow + i], 'column': shipColumn}
        positions.push(position);
      }
    } else {
      shipRow = context.getRandomInt(0, 9);
      shipColumn = context.getRandomInt(1, (context.columns.length - ship));
      for (let i = 0; i < ship; i++) {
        let position = {'row': context.rows[shipRow], 'column': shipColumn + i}
        positions.push(position);
      }
    }

    if (context.areRepeatedPositions(this.shipsPositions, positions)) {
      //Retry
      this.putShipOnBattleground(context, ship);
    } else {
      positions.map(position => this.shipsPositions.push(position));
    }
  }

  createBattleground() {
    let context = this;
    let recoveredBattleground = JSON.parse(window.localStorage.getItem('battleground'))
    if (!recoveredBattleground) {
      context.defineShipLocations()
      this.rows.map(row => {
        this.columns.map(column => {
          let position = {'row': row, 'column': column}
          if (context.haveShip(context.shipsPositions, position)) {
            this.battleground.push([row, column, 'ship']);
          } else {
            this.battleground.push([row, column, 'free']);
          }
        });
      });
      window.localStorage.setItem('battleground', JSON.stringify(this.battleground));
      window.localStorage.setItem('battlegroundSolution', JSON.stringify(this.battleground));
    } else {
      this.battleground = recoveredBattleground;
    }
  }

  haveShip(arr, val) {
    return arr.some(pos => pos.row == val.row && pos.column == val.column);
  }

  areRepeatedPositions(array, array2) {
    let result = false;
    array.map(first => {
      array2.map(second => {
        if (first.row == second.row && first.column == second.column) {
          result = true;
        }
      });
    });
    return result;
  }

  getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
  }

  failedClick() {
    if (this.player.attempts < 1) {
      this.gameOver();
    } else {
      this.player.attempts != 'INFINITE' ? this.player.attempts-- : 'INFINITE';
      this.player.failures++
      window.localStorage.setItem('gameInfo', JSON.stringify(this.player));
      this.showToast(this.failMessages[this.getRandomInt(0, (this.failMessages.length - 1 ))]);
    }
  }

  successClick() {
    if (this.player.attempts < 1) {
      this.gameOver();
    } else {
      this.player.attempts != 'INFINITE' ? this.player.attempts-- : 'INFINITE';
      this.player.hits++
      window.localStorage.setItem('gameInfo', JSON.stringify(this.player));
      this.showToast(this.successMessages[this.getRandomInt(0, (this.successMessages.length - 1 ))]);
    }
  }

  gameOver() {
    this.showToast('GAME OVER');
    this.battlegroundSolution = JSON.parse(window.localStorage.getItem('battlegroundSolution'));
    this.player.endTime = new Date();
    this.showSolution = true;
  }


  showToast(message) {
    this.toast.show(message, 4000);
  }

}
