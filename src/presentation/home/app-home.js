import {inject} from 'aurelia-framework';
import {Router} from 'aurelia-router';
import {MdToastService} from 'aurelia-materialize-bridge';

@inject(Router, MdToastService)
export class AppHome {

  noOldGamesSaved = true;

  constructor(router, toast) {
    this.router = router;
    this.toast = toast;
  }

  loadGame() {
    console.log('dsfds')
    let playerInfo = window.localStorage.getItem('gameInfo');
    let battleground = window.localStorage.getItem('battleground');
    if (playerInfo && battleground) {
      this.noOldGamesSaved = false;
      this.router.navigate('game');
    } else {
      this.toast.show('No previous games found', 4000);
    }
  }

}
