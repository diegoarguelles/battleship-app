import {bindable, inject} from 'aurelia-framework';
import {Router} from 'aurelia-router';

@inject(Router)
export class NewGameModal {

  @bindable() modalid = '';
  @bindable() playerName = '';
  @bindable() players = 1
  @bindable() attempts = 0
  blockAttemptsEditable = true;

  levels = [
    {
      'name': 'EASY',
      'attempts': 'INFINITE'
    },
    {
      'name': 'MEDIUM',
      'attempts': 100
    },
    {
      'name': 'HARD',
      'attempts': 50
    },
    {
      'name': 'CUSTOM',
      'attempts': 0
    }
  ];

  constructor(router){
    this.router = router;
  }

  attached() {
    $('#' + this.modalid).modal();
  }

  activateAttemptsInput(attempts) {
    if (attempts == -1) {
      this.blockAttemptsEditable = true;
    } else if (attempts == 0) {
      this.blockAttemptsEditable = false;
    } else {
      this.blockAttemptsEditable = true;
    }
  }

  createGame() {
    let game = {
      'playerName': this.playerName,
      'attempts': this.attempts,
      'startTime': new Date(),
      'hits': 0,
      'failures': 0
    }

    window.localStorage.clear();
    window.localStorage.setItem('gameInfo', JSON.stringify(game));
    this.router.navigate('game');
  }


}
