import {bindable} from 'aurelia-framework';

export class GameBoard {

  @bindable() battleground = [];
  @bindable() attemptFailedAction;
  @bindable() attemptSuccessAction;
  @bindable() type;

  getSection(section) {
    if (section[2] == 'clicked' || section[2] == 'destroyed') {
      //do anything
    } else if (section[2] == 'free') {
      this.updateBattlewground(section, 'clicked')
      this.attemptFailedAction();
    } else if (section[2] == 'ship') {
      this.updateBattlewground(section, 'destroyed')
      this.attemptSuccessAction();
    }
  }

  getBattlegroundFromLocalStorage() {
    return JSON.parse(window.localStorage.getItem('battleground'));
  }

  saveBattlegroundToLocalStorage(battleground) {
    window.localStorage.setItem('battleground', JSON.stringify(battleground));
  }

  updateBattlewground(section, status) {
    this.battleground = this.getBattlegroundFromLocalStorage();
    this.battleground.map(element => {
      if (element[0] == section[0] && element[1] == section[1]) {
        element[2] = status;
      }
    });
    this.saveBattlegroundToLocalStorage(this.battleground);
  }

}
