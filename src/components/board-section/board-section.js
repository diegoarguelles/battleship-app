import {bindable} from 'aurelia-framework';

export class BoardSection {

  @bindable() positionx = '';
  @bindable() positiony = '';
  @bindable() status = '';
  @bindable() className = '';
  @bindable() triggerAction;
  @bindable() boardtype = 'game';

  attached() {
    if (this.status == 'ship') {
      this.boardtype == 'game' ? this.className = 'empty' : this.className = 'ship';
    } else if (this.status == 'free') {
      this.className = 'empty';
    } else if (this.status == 'destroyed') {
      this.className = 'destroyed';
    }
  }

  someAction() {
    this.triggerAction();
  }


}
